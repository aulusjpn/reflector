﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage.Game.Components;
using Sandbox.ModAPI;
using Sandbox.Game.Weapons;
using Sandbox.Game.Entities;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Entities.Debris;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRageMath;
using VRage.Game.ModAPI.Interfaces;
using VRage.Utils;
using SpaceEngineers.Game.ModAPI.Ingame;
using TExtensions = Sandbox.ModAPI.Interfaces.TerminalPropertyExtensions;
using TActions = Sandbox.Game.Gui.TerminalActionExtensions;
using IMyGravityGenerator = SpaceEngineers.Game.ModAPI.IMyGravityGenerator;
using System.Collections;


namespace ReflecterCore {
	/// <summary>
	/// リフレクタークラス
	/// </summary>
	/// シールドレイヤーごとに生成。（検討中→シールド生成ロジックもここに？

	//// いっそIMyEntity継承してもいいと思うの
	public class Reflecter : IDisposable {

		
		/// <summary>
		/// リフレクタブシャットダウンEntity(参照になるのかなあ？
		/// </summary>
		public IMyEntity ReflectorEntity { get; private set; }
		
		/// <summary>
		/// 最大耐久力
		/// </summary>
		float maxHP;

		/// <summary>
		/// 今のHP
		/// </summary>
		float hp;

		
		/// <summary>
		/// 現在展開中か否か
		/// </summary>
		public bool isActive { get { return hp <= 0; } }

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="maxHP">最大耐久力</param>
		public Reflecter(long ownID, float maxHP) {
			// いろいろ初期化
			hp = 0;
			this.maxHP = maxHP;

			// 持ち主を更新
			setOwner(ownID);
		}

		/// <summary>
		/// ダメージを受ける
		/// </summary>
		/// <param name="damage">受けたダメージ</param>
		public void damage(float damage) {
			if((hp -= damage) <= 0) {
				// HPがなくなると死ぬ
				setReflectEntityState(false);
			}
		}

		/// <summary>
		/// リフレクタ復活か死亡の切り替え
		/// </summary>
		/// <param name="isActive">trueで復活</param>
		public void setActive(bool isActive) {

			if(isActive && this.isActive) {
				// 死んでて復活したいならHPを満タンにして復活
				hp = maxHP;
				setReflectEntityState(true);
			} else if(!isActive && !this.isActive) {
				// 生きていて死にたいならHP削って消す
				hp = 0f;
				setReflectEntityState(false);
			}
		}
		

		/// <summary>
		/// 開放用
		/// </summary>
		public void Dispose() {
			// 開放時に消しとく
			if(ReflectorEntity != null) {
				MyAPIGateway.Entities.RemoveEntity(ReflectorEntity);
				ReflectorEntity = null;
			}
		}

		/// <summary>
		/// 行列を更新
		/// </summary>
		/// <param name="mat">設定する行列</param>
		public void updateMatrix(MatrixD mat) {
			ReflectorEntity.WorldMatrix = mat;
		}

		/// <summary>
		/// シールドの状況を変更する
		/// 具体的には受け取ったboolの値に応じてentityの情報を書き換えといてください
		/// あうさんよろしく
		/// </summary>
		/// <param name="isActive">trueでentityが蘇って欲しい</param>
		private void setReflectEntityState(bool isActive) { }

		/// <summary>
		/// オーナーIDを更新
		/// あうさんﾖﾛﾋﾟｺ！！！！！！！！！
		/// </summary>
		public void setOwner(long ownID) {

		}

		/// <summary>
		/// デストラクタ
		/// </summary>
		~Reflecter() {
			Dispose();
		}


	}
}
