﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage.Game.Components;
using Sandbox.ModAPI;
using Sandbox.Game.Weapons;
using Sandbox.Game.Entities;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Entities.Debris;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRageMath;
using VRage.Game.ModAPI.Interfaces;
using VRage.Utils;
using SpaceEngineers.Game.ModAPI.Ingame;
using TExtensions = Sandbox.ModAPI.Interfaces.TerminalPropertyExtensions;
using TActions = Sandbox.Game.Gui.TerminalActionExtensions;
using IMyGravityGenerator = SpaceEngineers.Game.ModAPI.IMyGravityGenerator;
using System.Collections;


namespace ReflecterCore {

	/// <summary>
	/// 六角形な座標管理するクラス
	/// </summary>
	public class hexPos:IDisposable {

		/// <summary>
		/// コルーチン置き場
		/// </summary>
		IEnumerator<MatrixD> coloutinIns;

		/// <summary>
		/// 計算開始の起点
		/// </summary>
		MatrixD origin;

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="origin">生成する起点を記録しておく</param>
		public hexPos(MatrixD origin) {
			// 値を記録
			this.origin = origin;

			// コルーチン作成
			coloutinIns = coloutin();
		}

		/// <summary>
		/// いろいろ開放
		/// </summary>
		public void Dispose() {
			coloutinIns.Dispose();
		}

		/// <summary>
		/// 次の座標点をもらう
		/// </summary>
		/// <returns></returns>
		public MatrixD getNextPos() {

			// 進める前に値を覚えておかないと原点が取れないから真ん中に穴が開く！かも
			MatrixD tmp = coloutinIns.Current;

			// 次に進める
			coloutinIns.MoveNext();

			// 値を返す
			return tmp;
		}


		/// <summary>
		/// 計算用コルーチン
		/// </summary>
		/// <returns>コルーチンのインスタンス</returns>
		private IEnumerator<MatrixD> coloutin() {
			while(true) {
				// ここらへんでよろしく計算する
				yield return new MatrixD();
			}
		}
		
	}
}
