﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage.Game.Components;
using Sandbox.ModAPI;
using Sandbox.Game.Weapons;
using Sandbox.Game.Entities;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Entities.Debris;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRageMath;
using VRage.Game.ModAPI.Interfaces;
using VRage.Utils;
using SpaceEngineers.Game.ModAPI.Ingame;
using TExtensions = Sandbox.ModAPI.Interfaces.TerminalPropertyExtensions;
using TActions = Sandbox.Game.Gui.TerminalActionExtensions;
using IMyGravityGenerator = SpaceEngineers.Game.ModAPI.IMyGravityGenerator;
using System.Collections;

namespace ReflecterCore {
	public class Couple<T1, T2> {
		public T1 Item1 { get; set; }
		public T2 Item2 { get; set; }

		public Couple(T1 item1, T2 item2) {
			Item1 = item1;
			Item2 = item2;
		}
	}

	/// <summary>
	/// MyObjectBuilder_Projector
	/// プロジェクターブロックでsubtypenameが"TC_LargeShieldGenerator"に生成。
	/// インスタンスは各ブロックごとに生成される。
	/// （つまり、Entityは各ブロックごとにユニークになる）
	/// </summary>
	[MyEntityComponentDescriptor(typeof(MyObjectBuilder_Projector), new String[] { "TC_LargeShieldGenerator" })]
	class Generator : MyGameLogicComponent {

		/// <summary>
		/// 外部参照用
		/// </summary>
		static Dictionary<long, Generator> generators;
		
		//// こいつらReflecterの初期化時に引数で投げてみたけどなんかうまいこと調整して
		private const float ReflecterInitMoxPower = 0f;
		private const bool ReflecterInitFlg = false;

		/// <summary>
		/// 展開するべきリフレクタの数
		/// まあなんかうまいことやってくれ(
		/// </summary>
		private const uint numReflector = 10;

		private IMyProjector ReflectorGenerator;
		private IMyFunctionalBlock EntityFunctionalBlock;
		private IMyTerminalBlock EntityTerminalBlock;
		private MyCubeBlock EntityCubeBlock;

		private MyObjectBuilder_EntityBase m_objectBuilder = null;
		private bool m_init = false;
		private float anim_step = 0f;
		private int time;

		private float MaxEnergy = 1000000000f;
		private const float reload = 100f;
		private float Energy;

		/// <summary>
		/// リフレクタ置くためだけのコレクション
		/// </summary>
		multiReflector Reflectors;
		

		MatrixD ReflecterMatrix;

		private MatrixD WorldMatrix;

		private Vector4 color = Color.Red.ToVector4();
		private Vector3D Scale;

		//private BoundingSphereD sphere_min;
		//private BoundingSphereD sphere_max;

		private SerializableRange Range = new SerializableRange(0.75f, 1.05f);
		private SerializableRange RangeMeteor = new SerializableRange(0.75f, 1.35f);
		private SerializableRange RangeShip = new SerializableRange(0.925f, 1.005f);

		/// <summary>
		/// 外部参照用のインスタンス作るだけコンストラクタ
		/// </summary>
		static Generator() {
			generators = new Dictionary<long, Generator>();
		}

		/// <summary>
		/// 同一グリッド上にあるGeneratorを検索
		/// </summary>
		/// <param name="grid">検索対象のcubegrid</param>
		/// <returns>存在するgeneratorの一覧</returns>
		public static Generator[] findGeneratorInCubeGrid(IMyCubeGrid grid) {
			
			// 一覧取ってくる
			List<IMyEntity> inGrids = new List<IMyEntity>();
			grid.GetChildren(inGrids);

			// 同一グリッド上にあるものを検索
			lock(generators) {
				return generators.Where(e => inGrids.Find(f => f.EntityId == e.Key) != null).Select(e=>e.Value).ToArray();
			}
		}

		

		/// <summary>
		/// 初期化
		/// </summary>
		/// <param name="objectBuilder"></param>
		public override void Init(MyObjectBuilder_EntityBase objectBuilder) {
			//Entity = このクラスを生成したブロックのEntity。
			//objectBuilder　= 上と同じ、このブロックのMyObjectBuilder_EntityBase
			Entity.NeedsUpdate |= MyEntityUpdateEnum.EACH_FRAME;
			Entity.NeedsUpdate |= MyEntityUpdateEnum.EACH_10TH_FRAME;
			m_objectBuilder = objectBuilder;

			EntityFunctionalBlock = (IMyFunctionalBlock)Entity;
			ReflectorGenerator = (IMyProjector)Entity;
			EntityTerminalBlock = (IMyTerminalBlock)Entity;
			EntityCubeBlock = (MyCubeBlock)Entity;

			Reflectors = new multiReflector(EntityCubeBlock.OwnerId, ReflecterInitMoxPower);

			// 外部参照一覧につっこんどく
			lock(generators) {
				generators.Add(Entity.EntityId, this);
			}

		}

		/// <summary>
		/// 入り口
		/// ここが外部処理との出入口
		/// </summary>
		/// <param name="copy"></param>
		/// <returns></returns>
		public override MyObjectBuilder_EntityBase GetObjectBuilder(bool copy = false) {
			return m_objectBuilder;
		}

		/// <summary>
		/// close（破壊・消去・ゲーム終了時などブロックが消えるとき）処理
		/// </summary>
		public override void Close() {
			if(m_init) {

				// 全部開放
				Reflectors.ForEach(e => e.Dispose());

				// リストもきれいにしとく
				Reflectors.Clear();

				// 外部参照一覧からも消しとく
				lock(generators) {
					generators.Remove(Entity.EntityId);
				}

			}
		}

		/// <summary>
		/// 初期化処理
		/// </summary>
		private void Initialization() {
			//ShieldGenerator.LoadBlueprint("");

			//TExtensions.SetValueFloat(Entity as IMyTerminalBlock, "Width", 1f);
			//TExtensions.SetValueFloat(Entity as IMyTerminalBlock, "Depth", 1f);
			//TExtensions.SetValueFloat(Entity as IMyTerminalBlock, "Height", 1f);
			//Entity.Name = "TCShieldGenerator";
			//TExtensions.SetValueFloat(Entity as IMyTerminalBlock, "Gravity", 0.0001f);


			anim_step = 0f;



			//Energy = MaxEnergy;

			//WorldMatrix =ブロックの位置マトリックスを保持
			WorldMatrix = Entity.WorldMatrix;
			//WorldMatrix.Translation += Entity.WorldMatrix.Up * 0.35f;


			ReflecterMatrix = MatrixD.CreateWorld(Entity.PositionComp.GetPosition() + Entity.PositionComp.WorldMatrix.Up * 0, Entity.PositionComp.WorldMatrix.Forward, Entity.PositionComp.WorldMatrix.Up);
			//ReflecterMatrix = new MatrixD(WorldMatrix.Translation, Math.Max(ShieldGenerator.FieldDepth, Math.Max(ShieldGenerator.FieldHeight, ShieldGenerator.FieldWidth)));

			//デリゲート
			(Entity as IMyTerminalBlock).PropertiesChanged += Reflector_PropertiesChanged;
			//(Entity as IMyTerminalBlock).AppendingCustomInfo += Generator_AppendingCustomInfo;



			//Scale = new Vector3(EntityProjector.FieldDepth / 150f, EntityProjector.FieldHeight / 150f, EntityProjector.FieldWidth / 150f);
			//TExtensions.SetValueBool(Entity as IMyTerminalBlock, "ShowOnHUD", true);
			//Shield.Render.Visible = (Entity as IMyFunctionalBlock).ShowOnHUD;
			//TExtensions.SetValueFloat(Entity as IMyTerminalBlock, "Gravity", 0.0001f);

			//MaxEnergy = (float)Math.PI / 9 * (EntityProjector.FieldHeight * EntityProjector.FieldWidth * EntityProjector.FieldDepth);
			//Energy = MaxEnergy;

			//(Entity as MyCubeBlock).ResourceSink.SetRequiredInputByType((Entity as MyCubeBlock).ResourceSink.AcceptedResources.First(), (float)Math.PI / 6 * (EntityProjector.FieldHeight * EntityProjector.FieldWidth * EntityProjector.FieldDepth) * 0.0000001f);



			//// あっちこっちで弄るのはよくないと思う
			//// m_init = true;

		}

		/// <summary>
		/// シュミレート前更新処理
		/// </summary>
		public override void UpdateBeforeSimulation() {

			if(m_init) {
				//WorldMatrix = Entity.WorldMatrix;
				//WorldMatrix.Translation += Entity.WorldMatrix.Up * 0.35f;

				//if (Energy < 0)
				//{
				//    Energy = 0f;
				//}


				//Shield positionning
				//(Entity as IMyTerminalBlock).RefreshCustomInfo();
				//

				//// Reflectorsのlistを廃止したのと
				//// ReflectgorのShieldEntityを外からもアクセスできるようにしたのでそこらへん調整

				//暫定処理　ブロックの位置情報からシールド生成（向き座標*値で位置調整）
				//この処理はシュミレートに移行かな？そもそもシールド生成位置や大きさはそっちで設定
				MatrixD matrix = MatrixD.CreateWorld(Entity.PositionComp.GetPosition() + Entity.PositionComp.WorldMatrix.Up * 5, Entity.PositionComp.WorldMatrix.Forward, Entity.PositionComp.WorldMatrix.Up);
				//matrix = MatrixD.CreateFromTransformScale(Quaternion.CreateFromRotationMatrix(matrix.GetOrientation()), matrix.Translation, new Vector3D(1, 3, 2));
				//// Shields[0].SetWorldMatrix(matrix);
				//// Reflectors[0].SetWorldMatrix(matrix);
				// Shield_A.ShieldEntity.SetWorldMatrix(matrix);

				//暫定処理　ブロックの位置情報から右上に二枚目のシールド生成（向き座標*値で位置調整）
				// matrix = MatrixD.CreateWorld(Entity.PositionComp.GetPosition() + Entity.PositionComp.WorldMatrix.Up * 5 + Entity.PositionComp.WorldMatrix.Left * 3.8 + Entity.PositionComp.WorldMatrix.Forward * 6.6, Entity.PositionComp.WorldMatrix.Forward, Entity.PositionComp.WorldMatrix.Up);
				//// Reflectors[1].SetWorldMatrix(matrix);
				// Shield_B.ShieldEntity.SetWorldMatrix(matrix);
				//Shield.SetPosition(Entity.PositionComp.GetPosition() + Entity.PositionComp.WorldMatrix.Up * 5);

				// すべてのリフレクタに位置情報を投げる
				using(var pos = new hexPos(matrix))
					Reflectors.ForEach(e =>e.ReflectorEntity.SetWorldMatrix(pos.getNextPos()));
				
			}

		}


		

		/// <summary>
		/// シュミレート後更新処理
		/// </summary>
		public override void UpdateAfterSimulation() {

			if(!m_init) {
				//ブロックがシールド生成機がチェック・・・・だがこれ、そもそもいるのか？
				if(ReflectorGenerator.BlockDefinition.SubtypeId == "TC_LargeShieldGenerator") {
					//機能確認
					if(!EntityTerminalBlock.IsFunctional)
						return;

					// 指定数生成する
					Reflectors.setCount((int)numReflector);

					//元ネタより未修正
					MyAPIGateway.Session.DamageSystem.RegisterBeforeDamageHandler(100, DamageHandler);
					Initialization();
					m_init = true;
				} else {
					this.NeedsUpdate = MyEntityUpdateEnum.NONE;
				}
				return;
			}

			/*
			if(!(Entity as IMyFunctionalBlock).Enabled || !(Entity as IMyFunctionalBlock).IsFunctional) {
				//破損、または機能していないときに非描写に設定している
				foreach(var reflector in Reflectors)
					reflector.ReflectorEntity.Visible = false;
				return;
			} else {
				// 機能してる時は見えるようにしたほうがいいのかも
				foreach(var reflector in Reflectors)
					reflector.ReflectorEntity.Visible = true;
			}
			*/


			/*
			if(!(Entity as IMyFunctionalBlock).Enabled || !(Entity as IMyFunctionalBlock).IsFunctional) {
				//破損、または機能していないときはシャットダウン指示
				Reflectors.poweroff();
				return;
			}
			*/

			// 破損 or 機能停止中であればリフレクタをシャットダウン状態にしておく
			if(!(Entity as IMyFunctionalBlock).Enabled || !(Entity as IMyFunctionalBlock).IsFunctional)
				Reflectors.setActive(false);

			////Detection
			//float width = ShieldGenerator.FieldWidth;
			//float height = ShieldGenerator.FieldHeight;
			//float depth = ShieldGenerator.FieldDepth;


			EntityTerminalBlock.RefreshCustomInfo();


		}



		//HANDLERS

		/// <summary>
		/// ダメージハンドル
		/// ダメージ処理時にデリゲードで行う処理
		/// まだ書いてない
		/// </summary>
		/// <param name="target"></param>
		/// <param name="info"></param>
		private void DamageHandler(object target, ref MyDamageInformation info) {

            ////REXXAR: This is the proper use of 'as' casting
            //if (target == null)
            //{
            //    MyLog.Default.WriteLineAndConsole("target is null");
            //    return;
            //}

            var DamageBlock = target as IMySlimBlock;

            
            if (DamageBlock.GetObjectBuilder().SubtypeId.ToString() == "")
            {
                if (DamageBlock.CubeGrid == null)
                    {
                        MyLog.Default.WriteLineAndConsole("targetBlock is null");
                        return;
                    }
                }

                //if (targetBlock.CubeGrid.Name == null)
                //{
                //    MyLog.Default.WriteLineAndConsole("targetBlock.CubeGrid.Name is null");
                //    return;
                //}

                //if (targetBlock.CubeGrid.Name == String.Format("Impactor[{0}]", EntityCubeBlock.IDModule.Owner))
                //{
                //    Energy -= info.Amount;
                //}


            }

        /// <summary>
        /// デリゲード処理
        /// コンパネの右枠にある情報表示設定処理
        /// いる？
        /// </summary>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>

        //// あったほうがユーザビリティ向上！でもいらんなら消しちまえ！
        /*
        private void Generator_AppendingCustomInfo(IMyTerminalBlock arg1, StringBuilder arg2)
        {
            if (MyAPIGateway.Session.Config.Language == MyLanguagesEnum.French)
            {
                arg2.Append("Energie : " + Energy + "/" + MaxEnergy);
            }
            else
            {
                arg2.Append("Energy : " + Energy + "/" + MaxEnergy);
            }
        }
        */

        /// <summary>
        /// デリゲード処理
        /// コンパネで設定変更時におこなう処理
        /// </summary>
        /// <param name="obj"></param>
        private void Reflector_PropertiesChanged(IMyTerminalBlock obj) {
			//TExtensions.SetValueFloat(EntityTerminalBlock, "Gravity", 0.0001f);
			//Shield.Render.Visible = EntityFunctionalBlock.ShowOnHUD;
			//MaxEnergy = 2050f * (float)Math.PI / 6 * (ShieldGenerator.FieldHeight * ShieldGenerator.FieldWidth * ShieldGenerator.FieldDepth);
			//if (!EntityFunctionalBlock.Enabled)
			//{
			//    EntityCubeBlock.ResourceSink.SetRequiredInputByType(EntityCubeBlock.ResourceSink.AcceptedResources.First(), 0f);
			//}
			//else
			//{
			//    EntityCubeBlock.ResourceSink.SetRequiredInputByType(EntityCubeBlock.ResourceSink.AcceptedResources.First(), (float)Math.PI / 6 * (ShieldGenerator.FieldHeight * ShieldGenerator.FieldWidth * ShieldGenerator.FieldDepth) * 0.001f);
			//}
			//Scale = new Vector3(ShieldGenerator.FieldDepth / 150f, ShieldGenerator.FieldHeight / 150f, ShieldGenerator.FieldWidth / 150f);
		}

	}

}
