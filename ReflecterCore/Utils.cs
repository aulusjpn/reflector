﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage.Game.Components;
using Sandbox.ModAPI;
using Sandbox.Game.Weapons;
using Sandbox.Game.Entities;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Entities.Debris;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRageMath;
using VRage.Game.ModAPI.Interfaces;
using VRage.Utils;
using SpaceEngineers.Game.ModAPI.Ingame;
using TExtensions = Sandbox.ModAPI.Interfaces.TerminalPropertyExtensions;
using TActions = Sandbox.Game.Gui.TerminalActionExtensions;
using IMyGravityGenerator = SpaceEngineers.Game.ModAPI.IMyGravityGenerator;
using System.Collections;


namespace ReflecterCore
{
    public static class Utils
    {

        /// <summary>
        /// よく使うのでリフレクタもリフレクタでまとめてみよう
        /// </summary>
        /// <param name="ownID">オーナーのID</param>
        /// <returns>できたEntity</returns>
        public static IMyEntity SpawnReflect(long ownID)
        {
            return Utils.Spawn("TC_ShieldLarge", "", true, true, true, false, false, ownID);
        }

        //SPAWN METHOD
        /// <summary>
        /// 追記　月シールド流用　Entity生成メソッド
        /// 
        /// </summary>
        /// <param name="subtypeId">生成するブロックの種類</param>
        /// <param name="name">Grid名　空で問題なし</param>
        /// <param name="isVisible">描写フラグ</param>
        /// <param name="hasPhysics">物理判定フラグ</param>
        /// <param name="isStatic">静的物体フラグ</param>
        /// <param name="toSave">？？？？</param>
        /// <param name="destructible">非破壊・破壊フラグ</param>
        /// <param name="OwnerId">所有者ＩＤ（これを使えばタレットが誤射せずにすむ？）</param>
        /// <returns></returns>
        public static IMyEntity Spawn(string subtypeId, string name = "", bool isVisible = true, bool hasPhysics = false, bool isStatic = false, bool toSave = false, bool destructible = false, long OwnerId = 0)
        {

            CubeGridBuilder.Name = name;
            CubeGridBuilder.CubeBlocks[0].SubtypeName = subtypeId;
            CubeGridBuilder.CreatePhysics = hasPhysics;
            CubeGridBuilder.IsStatic = isStatic;
            CubeGridBuilder.DestructibleBlocks = destructible;
            IMyEntity ent = MyAPIGateway.Entities.CreateFromObjectBuilder(CubeGridBuilder);
            ent.Flags &= ~EntityFlags.Save;
            ent.Visible = isVisible;
            MyAPIGateway.Entities.AddEntity(ent, true);

            return ent;


        }

        //VARIABLES TO SPAWN
        //REXXAR: these vectors are unnecessary. You're wasting allocations
        //Vector3.Zero is a pre-defined struct that you can use without calling extra allocation
        //the Vector types can also be implicitly cast to the Serializable types, so you don't need to worry about it
        private static SerializableVector3 EntityVector0 = new SerializableVector3(0, 0, 0);
        private static SerializableVector3I EntityVectorI0 = new SerializableVector3I(0, 0, 0);


        //OBJECTBUILDERS

        /// <summary>
        /// グリッド生成用ビルダークラスの生成
        /// これを元ネタに各種設定を放り込んでGridを生成する
        /// </summary>
        private static MyObjectBuilder_CubeGrid CubeGridBuilder = new MyObjectBuilder_CubeGrid()
        {
            EntityId = 0,
            GridSizeEnum = MyCubeSize.Large,
            IsStatic = true,
            Skeleton = new List<BoneInfo>(),
            LinearVelocity = Vector3.Zero,
            AngularVelocity = Vector3.Zero,
            ConveyorLines = new List<MyObjectBuilder_ConveyorLine>(),
            BlockGroups = new List<MyObjectBuilder_BlockGroup>(),
            Handbrake = false,
            XMirroxPlane = null,
            YMirroxPlane = null,
            ZMirroxPlane = null,
            PersistentFlags = MyPersistentEntityFlags2.InScene,
            Name = "ReflecterCubeGrid",
            DisplayName = "",
            CreatePhysics = false,
            DestructibleBlocks = true,
            PositionAndOrientation = new MyPositionAndOrientation(Vector3D.Zero, Vector3D.Forward, Vector3D.Up),

            CubeBlocks = new List<MyObjectBuilder_CubeBlock>()
                {
                    new MyObjectBuilder_CubeBlock()
                    {
                        EntityId = 0,
                        BlockOrientation = new SerializableBlockOrientation(Base6Directions.Direction.Forward, Base6Directions.Direction.Up),
                        SubtypeName = "",
                        Min = Vector3I.Zero,
                        Owner = 0,
                        ShareMode = MyOwnershipShareModeEnum.None,
                        DeformationRatio = 0,
                    }
                }
        };
    }
}
