﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage.Game.Components;
using Sandbox.ModAPI;
using Sandbox.Game.Weapons;
using Sandbox.Game.Entities;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Entities.Debris;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRageMath;
using VRage.Game.ModAPI.Interfaces;
using VRage.Utils;
using SpaceEngineers.Game.ModAPI.Ingame;
using TExtensions = Sandbox.ModAPI.Interfaces.TerminalPropertyExtensions;
using TActions = Sandbox.Game.Gui.TerminalActionExtensions;
using IMyGravityGenerator = SpaceEngineers.Game.ModAPI.IMyGravityGenerator;
using System.Collections;

namespace ReflecterCore {
	

	/// <summary>
	/// 複数のリフレクタをステキに扱う妖怪listもどき
	/// </summary>
	class multiReflector:List<Reflecter>, IDisposable {
		/// <summary>
		/// オーナーID
		/// </summary>
		long ownID;

		/// <summary>
		/// 最大容量
		/// </summary>
		float maxPower { get; set; }

		/// <summary>
		/// リフレクタ生成数
		/// </summary>
		int reflectCount;

		// HP最大値
		float maxHP;

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="ownID">オーナID</param>
		/// <param name="maxHP">最大耐久力</param>
		public multiReflector(long ownID, float maxHP) {
			this.ownID = ownID;
			this.maxHP = maxHP;

			// 初期展開枚数は0
			reflectCount = 0;
		}


		/// <summary>
		/// オーナー情報更新
		/// </summary>
		/// <param name="ownID">新しい持ち主</param>
		public void updateOwner(long ownID) {
			ForEach(e => e.setOwner(ownID));
		}

		/// <summary>
		/// シールド生成枚数を設定
		/// </summary>
		/// <param name="num">生成する枚数</param>
		public void setCount(int num) {

			if(num <= 0) {
				// 全部消せと言うなら全部消す
				Clear();

			} else if(num > reflectCount) {
				// 増える
				for(int i = 0; i < num - reflectCount; i++)
					Add(new Reflecter(ownID, maxHP));
			} else if(num < reflectCount){
				// 減る
				RemoveRange(num - 1, Count);
			}
		}

		/// <summary>
		/// 死んだリフレクタを復旧させる
		/// </summary>
		/// <param name="num">復活させる枚数</param>
		/// <returns>復活させた数</returns>
		public int reGenReflector(int num) {
			return this.Where(e => !e.isActive).Take(num).Select(e => { e.setActive(true); return 1; }).Sum();
		}

		/// <summary>
		/// 行列の更新
		/// </summary>
		/// <param name="origin">行列の中心地点</param>
		public void updateMatrix(MatrixD origin) {
			using(var pos = new hexPos(origin))
				ForEach(e => e.updateMatrix(pos.getNextPos()));
		}

		/// <summary>
		/// 有効・無効切り替え
		/// </summary>
		/// <param name="isActive">有効ならtrue</param>
		public void setActive( bool isActive) {
			ForEach(e => e.setActive(isActive));
		}

		/// <summary>
		/// 死んだ時用
		/// </summary>
		public void Dispose() {
			// 全部開放しとく
			ForEach(e => e.Dispose());

			// そして空に
			Clear();
		}
		
		/// <summary>
		/// デストラクタ
		/// </summary>
		~multiReflector() {
			Dispose();
		}

	}
}
